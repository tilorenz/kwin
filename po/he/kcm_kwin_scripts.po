# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Diego Iastrubni <elcuco@kde.org>, 2012, 2013.
# elkana bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: kcm-kwin-scripts\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-21 02:02+0000\n"
"PO-Revision-Date: 2023-10-17 15:20+0300\n"
"Last-Translator: Elkana Bardugo <ttv200@gmail.com>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && "
"n % 10 == 0) ? 2 : 3));\n"
"X-Generator: Poedit 3.3.2\n"

#: module.cpp:50
#, kde-format
msgid "Import KWin Script"
msgstr "ייבוא סקריפט של KWin"

#: module.cpp:51
#, kde-format
msgid "*.kwinscript|KWin scripts (*.kwinscript)"
msgstr "*.kwinscript|סקריפטים של KWin‏ (‎*.kwinscript)"

#: module.cpp:62
#, kde-format
msgctxt "Placeholder is error message returned from the install service"
msgid ""
"Cannot import selected script.\n"
"%1"
msgstr ""
"אי אפשר לייבא את הסקריפט.\n"
"%1"

#: module.cpp:66
#, kde-format
msgctxt "Placeholder is name of the script that was imported"
msgid "The script \"%1\" was successfully imported."
msgstr "הסקריפט „%1” יובא בהצלחה."

#: module.cpp:125
#, kde-format
msgid "Error when uninstalling KWin Script: %1"
msgstr "שגיאה בהסרת סקריפט KWin:‏ %1"

#: ui/main.qml:23
#, kde-format
msgid "Install from File…"
msgstr "התקנה מקובץ…"

#: ui/main.qml:27
#, kde-format
msgctxt "@action:button get new KWin scripts"
msgid "Get New…"
msgstr "הורדת חדש…"

#: ui/main.qml:65
#, kde-format
msgctxt "@info:tooltip"
msgid "Delete…"
msgstr "מחיקה…"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "צוות התרגום של KDE ישראל"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "kde-l10n-he@kde.org"

#~ msgid "KWin Scripts"
#~ msgstr "סקריפטים של KWin"

#~ msgid "Configure KWin scripts"
#~ msgstr "הגדרה של סקריפטים של KWin"

#~ msgid "Tamás Krutki"
#~ msgstr "Tamás Krutki"

#~ msgid "KWin script configuration"
#~ msgstr "הגדרות של סקריפטים של KWin"

#~ msgid "Import KWin script..."
#~ msgstr "ייבוא סקריפט של KWin.."
